<?php
function ubah_huruf($string){
	//kode di sini
	$data = str_split($string);
	$result = "";
	foreach ($data as $row) {
		$karakter = $row;
		$next_karakter = ++$karakter; 
		if (strlen($next_karakter) > 1) 
		{
			$next_karakter = $next_karakter[0];
		}
		$result .= $next_karakter;	
	}
	return $result."<br>";

}

// TEST CASES
echo "wow => ".ubah_huruf('wow'); // xpx
echo "developer => ".ubah_huruf('developer'); // efwfmpqfs
echo "laravel => ".ubah_huruf('laravel'); // mbsbwfm
echo "keren => ".ubah_huruf('keren'); // lfsfo
echo "semangat => ".ubah_huruf('semangat'); // tfnbohbu

?>

