<?php
function tentukan_nilai($number)
{
    //  kode disini
	$msg = "";
	if($number >= 85 && $number <=100){
		$msg = "Sangat Baik <br>";
	}elseif($number >= 70 && $number <= 85){
		$msg = "Baik <br>";
	}elseif($number >= 60 && $number < 70){
		$msg = "Baik <br>";
	}else{
		$msg = "Kurang <br>";
	}
	return $msg;
}

//TEST CASES
echo "Nilai 98 memiliki nilai = ".tentukan_nilai(98); //Sangat Baik
echo "Nilai 76 memiliki nilai = ". tentukan_nilai(76); //Baik
echo "Nilai 67 memiliki nilai = ". tentukan_nilai(67); //Cukup
echo "Nilai 43 memiliki nilai = ". tentukan_nilai(43); //Kurang
?>