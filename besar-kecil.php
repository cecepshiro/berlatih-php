<?php
function tukar_besar_kecil($string){
	//kode di sini
	$data = str_split($string);
	$result = "";
	foreach ($data as $row) {	
		if(preg_match('/[A-Z]/', $row)){
			$result .= strtolower($row);
		}elseif(preg_match('/[a-z]/', $string)){
			$result .= strtoupper($row);
		}
	}
	return ($result)."<br>";
}

// TEST CASES
echo "Hello World => ".tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "I aM aLAY => ".tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "My Name is Bond!! => ".tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "IT sHOULD bE me => ".tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "001-A-3-5TrdYW => ".tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>